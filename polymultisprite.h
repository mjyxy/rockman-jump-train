#include <string>
#include "multisprite.h"

class PolyMultiSprite : public MultiSprite {
public:
  PolyMultiSprite(const std::string&, const std::string&);

  virtual ~PolyMultiSprite() { } 
  
  virtual const Frame* getFrame() const { return MultiSprite::getFrame(); }
  virtual void draw() const;

  virtual void update(Uint32 ticks);

private:
  int framewidth;
  int getDistance(const PolyMultiSprite*) const;
  PolyMultiSprite& operator=(const PolyMultiSprite&);
  PolyMultiSprite(const PolyMultiSprite& s);
};

