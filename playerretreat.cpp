#include "playerretreat.h"
#include "gamedata.h"

Retreat::Retreat(const std::string& name, const Vector2f& pos) :
  MultiSprite(name,pos, Vector2f(0,0)),  
  ciel("ciel",Vector2f(Gamedata::getInstance().getXmlInt("ciel/X"),Gamedata::getInstance().getXmlInt("ciel/Y")), Vector2f(0,0)),  
  sentence("retreatsentence",Vector2f(Gamedata::getInstance().getXmlInt("retreatsentence/X"),Gamedata::getInstance().getXmlInt("retreatsentence/Y")), Vector2f(0,0)),
  sentence2("retreatsentence2",Vector2f(Gamedata::getInstance().getXmlInt("retreatsentence2/X"),Gamedata::getInstance().getXmlInt("retreatsentence2/Y")), Vector2f(0,0)),  
  delay(Gamedata::getInstance().getXmlInt(name+"/delay")) 
  {
}

Retreat::~Retreat() { }

void Retreat::draw() const { 
  if(!MultiSprite::isend()){
    MultiSprite::draw();
  }else{
    MultiSprite::draw();
    ciel.drawinview();
    sentence.drawsentenceinview();
    if(sentence.isend()) sentence2.drawsentenceinview();
  }
}

void Retreat::update(Uint32 ticks) { 

  if(!MultiSprite::isend()){
    MultiSprite::update(ticks);
  }else if(!sentence.isend()){

    sentence.update(ticks);
  }else if(!sentence2.isend()){
    sentence2.update(ticks);
  }else{
    delay -= ticks;
  }

}

