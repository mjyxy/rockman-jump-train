#include "background.h"
#include "onewaymultisprite.h"

Background& Background::getInstance() {
  	static Background instance; 
  	return instance;
}


Background::Background() : tree("tree", Gamedata::getInstance().getXmlInt("tree/factor") ),
	far_sprites(),
  wall("wall", Gamedata::getInstance().getXmlInt("wall/factor") ),
  near_sprites()
{

  for(int i=0;i<Gamedata::getInstance().getXmlInt("bird1/num");i++){
    far_sprites.push_back( new OnewayMultiSprite("bird1",Gamedata::getInstance().getXmlFloat("bird1/factor")*i+Gamedata::getInstance().getXmlFloat("bird1/index")) );
  }
  for(int i=0;i<Gamedata::getInstance().getXmlInt("bird2/num");i++){
    far_sprites.push_back( new OnewayMultiSprite("bird2",Gamedata::getInstance().getXmlFloat("bird2/factor")*i+Gamedata::getInstance().getXmlFloat("bird2/index")) );
  }
  for(int i=0;i<Gamedata::getInstance().getXmlInt("bird3/num");i++){
    near_sprites.push_back( new OnewayMultiSprite("bird3",Gamedata::getInstance().getXmlFloat("bird3/factor")*i+Gamedata::getInstance().getXmlFloat("bird3/index")) );
  }  

}


void Background::draw() const {
  	tree.draw();
  	std::list<Drawable*>::const_iterator ptr = far_sprites.begin();
  	while ( ptr != far_sprites.end() ) {
    	(*ptr)->draw();
    	++ptr;
  	}

  	ptr = near_sprites.begin();
  	while ( ptr != near_sprites.end() ) {
    	(*ptr)->draw();
    	++ptr;
  	}
    wall.draw();    
}
void Background::update(Uint32 ticks){
	tree.update();
  	std::list<Drawable*>::const_iterator ptr = far_sprites.begin();
  	while ( ptr != far_sprites.end() ) {
    	(*ptr)->update(ticks);
    	++ptr;
  	}

  	ptr = near_sprites.begin();
  	while ( ptr != near_sprites.end() ) {
    	(*ptr)->update(ticks);
    	++ptr;
  	}
    wall.update();
}
