#include <iostream>
#include <cmath>
#include "collisionStrategy.h"
#include "bullet.h"

void Bullet::update(Uint32 ticks) { 
  float yincr = velocityY() * static_cast<float>(ticks) * 0.001;
  Y( Y()- yincr );
  float xincr = velocityX() * static_cast<float>(ticks) * 0.001;
  X( X()- xincr );
  distance += ( hypot(xincr, yincr) );
  if (distance > maxDistance) tooFar = true;
}

Bullet::Bullet(
    const Vector2f& pos, const Vector2f vel, 
    const std::string& name, const Frame* fm, float dis ) :
    
    Sprite(name, pos, vel, fm),
    strategy(new RectangularCollisionStrategy), 
    distance(0), 
    maxDistance(dis), 
    tooFar(false) 
{ 
    if(maxDistance == -1) maxDistance = Gamedata::getInstance().getXmlInt(name+"/distance");
}

bool Bullet::collidedWith(const Drawable* d) const {
	return strategy->execute(*this, *d);
}

Bullet::~Bullet() { delete strategy; } 
