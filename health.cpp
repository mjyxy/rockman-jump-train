#include "health.h"
#include "gamedata.h"
#include "viewport.h"

void Health::drawHealth(SDL_Surface* screen) {
	// const Uint32 YELLOW = SDL_MapRGB(screen->format, 0xff, 0xff, 0x52);
	const Uint32 GREEN = SDL_MapRGB(screen->format, 0x63, 0xff, 0x84);
	const Uint32 WIGHT = SDL_MapRGB(screen->format, 0xff, 0xff, 0xff);
  	Draw_AALine(screen, x,y, x+width,y, height, WIGHT);
  	Draw_AALine(screen, x+curhealth, y, x+width, y, height, GREEN);
  	box.draw(Viewport::getInstance().X()+box.X(), box.Y());
}

void Health::deHealth(int d){
  curhealth += d*variation;
}

void Health::reset(){
  curhealth = 0;
}

Health& Health::getInstance() {
  	static Health instance; 
  	return instance;
}

Health::Health() :
	box(Sprite("health")),
	variation(Gamedata::getInstance().getXmlInt("health/variation")),
	width(Gamedata::getInstance().getXmlInt("health/w")),
	height(Gamedata::getInstance().getXmlInt("health/h")),
	x(Gamedata::getInstance().getXmlInt("health/x")),
	y(Gamedata::getInstance().getXmlInt("health/y")),
	curhealth(0)		// max
{

}
