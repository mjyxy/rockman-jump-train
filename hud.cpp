#include "hud.h"
#include "gamedata.h"
#include "clock.h"
#include "viewport.h"
#include <iostream>
#include "bulletpool.h"

void Hud::drawHUD(SDL_Surface* screen) {
	if(timer){
		const Uint32 BLUE = SDL_MapRGB(screen->format, 0, 0, 0xff);
	  	Draw_AALine(screen, x, y+height/2, 
	                    x+width,y+height/2, 
	                    height, 0xff, 0xff, 0xff, 0xff/2);
	  	Draw_AALine(screen, x,y, x+width,y, BLUE);
	  	Draw_AALine(screen, x,y+height, x+width,y+height, BLUE);
	  	Draw_AALine(screen, x,y, x,y+height, BLUE);
	  	Draw_AALine(screen, x+width,y, x+width,y+height, BLUE);
	  	int index = Gamedata::getInstance().getXmlInt("hud/index"),factor = Gamedata::getInstance().getXmlInt("hud/factor"), i = 0;
	  	
  		Clock::getInstance().draw(x+index,y+index+factor*(i++));
  		i+=2;
  		Viewport::getInstance().draw(x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("A D moves rockman", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("J jump", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("K shoot", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("R reset", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("G god mode", x+index, y+index+factor*(i++));
  		i+=1;
  		IOManager::getInstance().printMessageAt("F1 shows HUD", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("F4 prints screen", x+index, y+index+factor*(i++));
  		IOManager::getInstance().printMessageAt("P pause / S slowmode", x+index, y+index+factor*(i++));

  		IOManager::getInstance().printMessageValueAt("Bulletpool: ",BulletPool::getInstance().getbulletnum(), x+index, y+index+factor*(i++));
		IOManager::getInstance().printMessageValueAt("Freelist pool: ",BulletPool::getInstance().getfreelistnum(), x+index, y+index+factor*(i++));
	  	timer -= variation;
	}
}

void Hud::toggleHUD(){
	if(timer) 	timer = 0;
	else	timer = up_bound;
}

Hud& Hud::getInstance() {
  	static Hud instance; 
  	return instance;
}

Hud::Hud() :
	timer(Gamedata::getInstance().getXmlInt("hud/up_bound")),
	up_bound(Gamedata::getInstance().getXmlInt("hud/up_bound")),
	variation(Gamedata::getInstance().getXmlInt("hud/variation")),
	width(Gamedata::getInstance().getXmlInt("hud/width")),
	height(Gamedata::getInstance().getXmlInt("hud/height")),
	x(Gamedata::getInstance().getXmlInt("hud/x")),
	y(Gamedata::getInstance().getXmlInt("hud/y"))
{

}
