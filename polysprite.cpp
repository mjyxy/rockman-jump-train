#include <cmath>
#include "polysprite.h"
#include "gamedata.h"
#include "frameFactory.h"
#include "viewport.h"

PolySprite::PolySprite(const std::string& name, const std::string& name2):
  Sprite(name),
  framewidth(Gamedata::getInstance().getXmlInt(name2+"/width"))
{ }


PolySprite::PolySprite(const PolySprite& s) :
  Sprite(s),
  framewidth(s.framewidth)
{ }

PolySprite& PolySprite::operator=(const PolySprite& rhs) {
  Sprite::operator=( rhs );
  framewidth = rhs.framewidth;
  return *this;
}


void PolySprite::draw() const { 
  Sprite::draw(); 
  Sprite::draw(X()+framewidth,Y());
}

int PolySprite::getDistance(const PolySprite *obj) const { 
  return hypot(X()-obj->X(), Y()-obj->Y());
}

void PolySprite::update(Uint32 ticks) { 

  float tempX = Viewport::getInstance().X();

  while(X()<((int)tempX) % framewidth){
    X(X()+framewidth);
  }
  X(((int)X()%framewidth)-((int)tempX) % framewidth+tempX);
  Sprite::update(ticks);

}
