#include "story.h"
#include "gamedata.h"

Story::Story(const std::string& name, const std::string& name2, const std::string& name3) :
  ciel("ciel",Vector2f(Gamedata::getInstance().getXmlInt("ciel/X"),Gamedata::getInstance().getXmlInt("ciel/Y")), Vector2f(0,0)),  
  sentence(name,Vector2f(Gamedata::getInstance().getXmlInt(name+"/X"),Gamedata::getInstance().getXmlInt(name+"/Y")), Vector2f(0,0)),
  sentence2(name2,Vector2f(Gamedata::getInstance().getXmlInt(name2+"/X"),Gamedata::getInstance().getXmlInt(name2+"/Y")), Vector2f(0,0)),  
  sentence3(name3,Vector2f(Gamedata::getInstance().getXmlInt(name3+"/X"),Gamedata::getInstance().getXmlInt(name3+"/Y")), Vector2f(0,0)), 
  delay(Gamedata::getInstance().getXmlInt(name+"/delay")) 
  {
}

void Story::draw() const { 
  ciel.drawinview();
  sentence.drawsentenceinview();
  if(sentence.isend()) sentence2.drawsentenceinview();
  if(sentence2.isend()) sentence3.drawsentenceinview();
}

void Story::update(Uint32 ticks) { 
  if(!sentence.isend()){
    sentence.update(ticks);
  }else if(!sentence2.isend()){
    sentence2.update(ticks);
  }else if(!sentence3.isend()){
    sentence3.update(ticks);
  }else{
    delay -= ticks;
  }

}

