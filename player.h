#ifndef Player__H
#define Player__H
#include <string>
#include <vector>
#include "drawable.h"
#include "collisionStrategy.h"

class Retreat;

class Player : public Drawable {
public:
  Player(const std::string&, const double angle = 0, const double size = 1);
  Player(const std::string&, const Vector2f&, const Vector2f&, const double angle = 0, const double size = 1);
  virtual ~Player();

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks, const Drawable*);
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return standcollisionframe; 
  }


  bool collidedWith(const Drawable* d) const {
    return strategy2->execute(*this, *d);
  }

  int collidedWith2(const Drawable* d) const {
    return strategy2->collisionatbottom(*this, *d);
  }
  int collidedWithSide(const Drawable* d) const {
    return strategy2->collisionatside(*this, *d);
  }

  bool collidedWithLadder(const Drawable* d) const {
    return strategy1->execute(*this, *d);
  }

  bool collidedWithEnemy(const Drawable* d) const {
    if(imurfather) return false;
    if(godtimer) return false;
    return strategy1->execute(*this, *d);
  }

  bool collidedWithTrap(const Drawable* d) const {
    if(imurfather) return false;
    if(godtimer) return false;
    return strategy1->executepoly(*this, *d);
  }

  void explode();
  void eleexplode();
  void moveright();
  void moveleft();
  void notmove();
  void shoot();
  void stopshoot();
  void jump(); 
  void reset();
  void resetflag();
  void reposition() { needtoreposition = true; }
  void god(){ imurfather = !imurfather; }
  bool isgod() const { return imurfather||godtimer; }
  bool isexploded() const { if ( explosion ) return true; return false; }
  void fullgodtimer() { godtimer = 120; }
protected:
  Retreat* explosion;
  const std::vector<Frame *> right_stand_frames;
  const std::vector<Frame *> right_walk_frames;
  const std::vector<Frame *> right_jump_frames;

  const std::vector<Frame *> left_stand_frames;
  const std::vector<Frame *> left_walk_frames;
  const std::vector<Frame *> left_jump_frames;


  const std::vector<Frame *> stand_shoot_frames;
  const std::vector<Frame *> right_walk_shoot_frames;
  const std::vector<Frame *> left_walk_shoot_frames;
  const std::vector<Frame *> right_jump_shoot_frames;
  const std::vector<Frame *> left_jump_shoot_frames;


  Frame* standcollisionframe;
  Frame* jumpcollisionframe;

  int worldWidth;
  int worldHeight;

  unsigned currentFrame;
  unsigned numberOfstandFrames;
  unsigned numberOfwalkFrames;
  unsigned numberOfjumpFrames;

  unsigned standframeInterval;
  unsigned walkframeInterval;
  unsigned jumpframeInterval;
  unsigned bulletInterval;

  float timeSinceLastFrame;
  float bullettime;
  int walkframeWidth;   // walk==jump==stand
  int jumpframeHeight;    // jump==walk==stand

  bool preright;  // face to right
  bool isshoot;
  bool imurfather;
  bool needtoreposition;
  int isjump;   // used to save jump state
  int prestate;   // used to save prestate, 0 stand, 1 walk, 2 jump. For pause
  bool ispause;    // pause flag
  int movespeed;
  int jumpspeed;
  int jumpacc;
  int godtimer;
  bool isfall;
  PerPixelCollisionStrategy * strategy1;
  MyCollisionStrategy * strategy2;


  void standadvanceFrame(Uint32 ticks);
  void walkadvanceFrame(Uint32 ticks, const Drawable*);
  void walkadvanceFrame(Uint32 ticks);
  void jumpadvanceFrame(Uint32 ticks, const Drawable*);
  void jumpadvanceFrame(Uint32 ticks);
  void falladvanceFrame(Uint32 ticks, const Drawable*);
  void falladvanceFrame(Uint32 ticks);
  Player(const Player&);
  Player& operator=(const Player&);
};
#endif
