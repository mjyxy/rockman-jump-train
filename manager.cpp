#include <iostream>
#include <string>
#include <iomanip>
#include "vector2f.h"
#include "multisprite.h"
#include "sound.h"
#include "twowayintermultisprite.h"
#include "aroundmultisprite.h"
#include "sprite.h"
#include "gamedata.h"
#include "manager.h"
#include "extractSurface.h"


Manager::~Manager() { 
  if(beginstory) delete beginstory;
}

Manager::Manager() :
  env( SDL_putenv(const_cast<char*>("SDL_VIDEO_CENTERED=center")) ),
  io( IOManager::getInstance() ),
  clock( Clock::getInstance() ),
  screen( io.getScreen() ),

  background( Background::getInstance()),
  foreground( Foreground::getInstance()),
  beginstory(new Story("retreatsentence","begin1","begin2")),
  makeVideo( false ),
  frameCount( 0 ),
  username(  Gamedata::getInstance().getXmlStr("username") ),
  title( Gamedata::getInstance().getXmlStr("screenTitle") ),
  frameMax( Gamedata::getInstance().getXmlInt("frameMax") )

{
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
    throw string("Unable to initialize SDL: ");
  }
  SDL_WM_SetCaption(title.c_str(), NULL);
  atexit(SDL_Quit);

}

void Manager::draw() const {

  background.draw();
  foreground.draw();
  if(beginstory) beginstory->draw();
  io.printMessageAt(title, 10, 450);
  Hud::getInstance().drawHUD(screen);
  SDL_Flip(screen);
}

// Move this to IOManager, maybe it is not a good idea
void Manager::makeFrame() {
  std::stringstream strm;
  strm << "frames/" << username<< '.' 
       << std::setfill('0') << std::setw(4) 
       << frameCount++ << ".bmp";
  std::string filename( strm.str() );
  std::cout << "Making frame: " << filename << std::endl;
  SDL_SaveBMP(screen, filename.c_str());
}


void Manager::update() {
  clock.update();
  Uint32 ticks = clock.getTicksSinceLastFrame();
  if ( makeVideo && frameCount < frameMax ) {
    makeFrame();
  }
  background.update(ticks);
  foreground.update(ticks);
  if(beginstory){
    beginstory->update(ticks);
    if(beginstory->end()){
      delete beginstory;
      beginstory = NULL;
    }
  }
}

void Manager::reset(){
  clock.reset();
  // background don't need reset
  if(beginstory){
    delete beginstory;
    beginstory = NULL;
  }
  beginstory = new Story("retreatsentence","begin1","begin2");
  foreground.reset();
}

void Manager::play() {
  SDL_Event event;
  bool done = false;
  clock.start();
  
  // while(!beginstory->end()){   // this version is show story and player can't move
  //   draw();
  //   update();
  // }
  // delete beginstory;
  // beginstory = NULL;
  SDLSound sound;
  while ( not done ) {
    while ( SDL_PollEvent(&event) ) {
      Uint8 *keystate = SDL_GetKeyState(NULL);
      if (event.type ==  SDL_QUIT) { done = true; break; }
      if(event.type == SDL_KEYDOWN) {     // check key down
        if (keystate[SDLK_ESCAPE] || keystate[SDLK_q]) {
          done = true;
          break;
        }
        if ( keystate[SDLK_p] ) {
          sound.toggleMusic();
          if ( clock.isPaused() ) clock.unpause();
          else clock.pause();
        }
        if (keystate[SDLK_s]) {
          clock.toggleSloMo();
        }
        if (keystate[SDLK_F4] && !makeVideo) {
          std::cout << "Making video frames" << std::endl;
          makeVideo = true;
        }

        if (keystate[SDLK_j]) {
          foreground.playerjump();
        }

        if (keystate[SDLK_d] && !keystate[SDLK_a]) {
          foreground.playermoveright();
        }

        if(keystate[SDLK_a] && !keystate[SDLK_d]) {
          foreground.playermoveleft();
        }
        if (!keystate[SDLK_d] && !keystate[SDLK_a]) {
          foreground.playernotmove();
        }
        if (keystate[SDLK_d] && keystate[SDLK_a]) {
          foreground.playernotmove();
        }
        if (keystate[SDLK_F1]) {
          Hud::getInstance().toggleHUD();
        }
        if (keystate[SDLK_k]) {
          foreground.playershoot();
        }

        if (keystate[SDLK_r]) {
          reset();
        }
        if (keystate[SDLK_g]) {
          foreground.playergod();
        }
        // if (keystate[SDLK_SPACE]) {
        //   foreground.toggleMusic();
        // } 
      }

      if(event.type == SDL_KEYUP) {     // check key up
        if (keystate[SDLK_d] && !keystate[SDLK_a]) {
            foreground.playermoveright();
        }

        if(keystate[SDLK_a] && !keystate[SDLK_d]) {
            foreground.playermoveleft();
        }

        if(!keystate[SDLK_a] && !keystate[SDLK_d]) {
            foreground.playernotmove();
        }
        if(keystate[SDLK_a] && keystate[SDLK_d]) {
            foreground.playernotmove();
        }
        if (!keystate[SDLK_k]) {
          foreground.playerstopshoot();
        }
      }
    }
    if(foreground.isshoot()) {
      sound[0];
      foreground.falseshoot();
    }
    while(foreground.enemyexplode()>0) {
      sound[1];
      foreground.explodedone();
    }
    draw();
    update();
  }
}
