#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include "clock.h"
#include "gamedata.h"
#include "ioManager.h"

Clock& Clock::getInstance() {
  if ( SDL_WasInit(SDL_INIT_VIDEO) == 0) {
    throw std::string("Must init SDL before Clock");
  }
  static Clock clock; 
  return clock;
}

Clock::Clock() :
  ticks(0),
  totalTicks(0),
  started(false), 
  paused(false), 
  sloMo(false), 
  sumOfTicks(SDL_GetTicks()),
  shouldtick(1000/Gamedata::getInstance().getXmlInt("frameCap")),
  pausegap(0),
  lastsum(0),
  TicksPerTenFrames(0),
  preTicks(0),
  index(0),
  slowfac(Gamedata::getInstance().getXmlInt("slowFactor"))
  {
  start();
}

void Clock::draw(Uint32 x, Uint32 y) const { 
  IOManager::getInstance().
    printMessageValueAt("Seconds: ", getSeconds(), x, y);
  IOManager::getInstance().
    printMessageValueAt("Fps: ", getFps(), x, y+20);
}

void Clock::update() { 
  lastsum = totalTicks;
  totalTicks = SDL_GetTicks();
  if(!paused) ticks = totalTicks - sumOfTicks - pausegap;
  else ticks = totalTicks - lastsum;

  if(ticks < (index%2 ? shouldtick:shouldtick+1)) SDL_Delay((index%2 ? shouldtick:shouldtick+1) - ticks );
  totalTicks += (index%2 ? shouldtick:shouldtick+1) - ticks;

  if(index/10){
    index = 0;
    SetTicksSinceLastTenFrames(totalTicks);
  }
  index++;

  if(paused) ticks =0;
  else{
    ticks = totalTicks - sumOfTicks - pausegap;
    sumOfTicks += ticks;
    if(sloMo) ticks = ticks/slowfac;
  }

}

unsigned int Clock::getTicksSinceLastFrame() const {
  return ticks;
}

void Clock::SetTicksSinceLastTenFrames(const unsigned int &rhs) {
  TicksPerTenFrames = rhs - preTicks;
  preTicks = rhs;
}

void Clock::toggleSloMo() {
  sloMo = ! sloMo;
}

int Clock::getFps() const { 

  if(TicksPerTenFrames > 0) return 10000/TicksPerTenFrames;
  return 0;

}


void Clock::start() { 
  started = true; 
  paused = false; 
}

void Clock::pause() { 
  paused = true;
}

void Clock::unpause() { 
  paused = false; 
  pausegap = SDL_GetTicks() - sumOfTicks;  //compute time gap
}

void Clock::reset(){
  pausegap = pausegap+sumOfTicks;  // saved reset time;
  ticks = 0;
  totalTicks = 0;
  started = false; 
  paused = false;
  sloMo = false; 
  sumOfTicks = 0;
  lastsum = 0;
  TicksPerTenFrames = 0;
  preTicks = 0;
  index = 0;
}
