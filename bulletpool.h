#include <iostream>
#include <string>
#include <list>
#include "vector2f.h"
#include "bullet.h"
#include "gamedata.h"

class BulletPool {
public:
	static BulletPool& getInstance();  // This class is a Singleton
	void update(Uint32);
	void draw();
	void playershoot(const Vector2f& pos, const Vector2f& vel, float dis=-1);
	unsigned getbulletnum() { return playerbullet.size(); }
	unsigned getfreelistnum() { return playerfreelist.size(); }
	bool collidedWith(const Drawable*);
	void reset(){
		std::list<Bullet*>::iterator ptr = playerbullet.begin();
		while(ptr != playerbullet.end()){
			delete (*ptr);
			++ptr;
		}
		ptr = playerfreelist.begin();
		while(ptr != playerfreelist.end()){
			delete (*ptr);
			++ptr;
		}
		playerbullet.clear();
		playerfreelist.clear();
	}
private:
	std::list<Bullet*> playerbullet;		
	std::list<Bullet*> playerfreelist;
	unsigned int playermaxbullet;

	BulletPool() : playerbullet(), playerfreelist(), playermaxbullet(Gamedata::getInstance().getXmlInt("bulletpool/playerbullet"))
	{}
	~BulletPool() {
		std::list<Bullet*>::iterator ptr = playerbullet.begin();
		while(ptr != playerbullet.end()){
			delete (*ptr);
			++ptr;
		}
		ptr = playerfreelist.begin();
		while(ptr != playerfreelist.end()){
			delete (*ptr);
			++ptr;
		}
	}
	BulletPool(const BulletPool&);
  	BulletPool&operator=(const BulletPool&);
};
