#include "twowaymultisprite.h"
#include "gamedata.h"
#include "frameFactory.h"
#include "explodingSprite.h"

TwowayMultiSprite::~TwowayMultiSprite(){
  if (explosion) delete explosion;
}

void TwowayMultiSprite::advanceFrame(Uint32 ticks) {
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame+1) % numberOfFrames;
    timeSinceLastFrame = 0;
  }
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( Y() < 0) {
    velocityY( abs( velocityY() ) );
  }
  if ( Y() > worldHeight-frameHeight) {
    velocityY( -abs( velocityY() ) );
  }

  if ( X() < 0) {
    if(!notreverse){
      notreverse = true;
      velocityX( abs( velocityX() ) );
    }
  }
  if ( X() > worldWidth-frameWidth) {
    if(notreverse){
      notreverse = false;
      velocityX( -abs( velocityX() ) );
    }
  }  
}

TwowayMultiSprite::TwowayMultiSprite( const std::string& name ,const std::string& name2,const double angle, const double size) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  explosion(NULL),
  frames( FrameFactory::getInstance().getFrames(name,angle,size) ),
  revframes( FrameFactory::getInstance().getFrames(name2,angle,size) ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval") ),
  timeSinceLastFrame( 0 ),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  notreverse(true)
{ }

TwowayMultiSprite::TwowayMultiSprite( const std::string& name,const std::string& name2,const Vector2f& pos, const Vector2f& vel, const double angle, const double size) :
  Drawable(name, pos, vel),
  explosion(NULL),
  frames( FrameFactory::getInstance().getFrames(name,angle,size) ),
  revframes( FrameFactory::getInstance().getFrames(name2,angle,size) ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval") ),
  timeSinceLastFrame( 0 ),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  notreverse(true)
{ }

void TwowayMultiSprite::reset(){ 

  if (explosion) {
    delete explosion;
    explosion = NULL;
  }  
  currentFrame = 0;
  timeSinceLastFrame = 0;
  notreverse = true;

}

void TwowayMultiSprite::draw() const { 
  if (explosion) {
    explosion->draw();
    return;
  }  
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  if( notreverse )
  frames[currentFrame]->draw(x, y);
  else revframes[currentFrame]->draw(x, y);
}

void TwowayMultiSprite::explode() { 
  if ( explosion ) return;
  Sprite sprite(getName(), getPosition(), getVelocity(), getFrame());
  explosion = new ExplodingSprite(sprite); 
}

void TwowayMultiSprite::draw(Uint32 x, Uint32 y) const { 
  if (explosion) {
    explosion->draw();
    return;
  }  
  if( notreverse )  frames[currentFrame]->draw(x, y);
  else revframes[currentFrame]->draw(x, y);
}

void TwowayMultiSprite::update(Uint32 ticks) { 
  if ( explosion ) {
    explosion->update(ticks);
    if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
    }
    return;
  }  
  advanceFrame(ticks);
}
