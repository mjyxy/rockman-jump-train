#include <string>
#include <vector>
#include "vector2f.h"
#include "twowaymultisprite.h"
#include "player.h"

class SmartEnemy : public TwowayMultiSprite {
public:
  SmartEnemy(const std::string&, const std::string&, const std::string&, const std::string&,  int, int, const Player& );
  virtual ~SmartEnemy() { } 

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  void reset();
  int findplayer(){
    if(player.isexploded()) return 0;
    if(player.isgod()) return 0;
    if(abs((player.Y() + player.getFrame()->getHeight()) - (Y()+getFrame()->getHeight())) > 3){
      return 0;
    }
    int temp = player.X() - X();
    if(notreverse){// seek right
      if(temp>0 && temp<seekDistance) return 1;
    }else{//seek left
      if(temp<0 && abs(temp) < seekDistance) return -1;
    }
    return 0;
  }

  void impactright();
  void impactleft();
protected:
  enum MODE {NORMAL, IMPACT};

  const Player & player;
  float seekDistance;
  int impactSpeed;
  MODE currentMode;

  const std::vector<Frame *> left_impact_frames;
  const std::vector<Frame *> right_impact_frames;
  int numberOfFrames;
  bool notreverse;
  int inter;
  int extend;
  
  int centra;
  int radius;
  
  void advanceFrame(Uint32 ticks);
  SmartEnemy(const SmartEnemy&);
  SmartEnemy& operator=(const SmartEnemy&);
};
