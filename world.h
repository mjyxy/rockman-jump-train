#include <string>
#include "ioManager.h"
#include "frame.h"
#include "viewport.h"
#include "frameFactory.h"

class World {
public:
  World(const std::string& name, int fact);
  void update();
  void draw() const;

private:
  Frame* frame;
  int factor;
  unsigned frameWidth;
  unsigned worldWidth;
  float viewX;
  float viewY;
  const Viewport & view;
  World(const World&);
  World& operator=(const World&);
};

