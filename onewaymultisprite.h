#ifndef ONEWAYMULTISPRITE__H
#define ONEWAYMULTISPRITE__H
#include <string>
#include <vector>
#include "drawable.h"
#include <sstream>

class ExplodingSprite;

class OnewayMultiSprite : public Drawable {
public:
  OnewayMultiSprite(const std::string&, const double size = 1);
  virtual ~OnewayMultiSprite() { } 

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  void explode();
  bool isexploded () const {
    if(explosion) return true;
    return false;
  }
private:
  ExplodingSprite* explosion;
  const std::vector<Frame *> frames;
  int worldWidth;
  int worldHeight;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int frameWidth;
  int frameHeight;

  void advanceFrame(Uint32 ticks);
  OnewayMultiSprite(const OnewayMultiSprite&);
  OnewayMultiSprite& operator=(const OnewayMultiSprite&);
};

#endif


