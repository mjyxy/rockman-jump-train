#include "frameFactory.h"
#include "extractSurface.h"
#include "ioManager.h"
#include "vector2f.h"
#include <SDL.h>
#include <SDL_rotozoom.h>
#include <math.h>
#include <sstream>

FrameFactory::~FrameFactory() {
  // Free elements of signle fram sprites
  std::map<std::string, Frame*>::iterator delsingleframes = frames.begin();
  while(delsingleframes != frames.end()){
    delete delsingleframes->second;
    delsingleframes++;
  }

  std::map<std::string, SDL_Surface*>::iterator delsinglesurf = surfaces.begin();
  while(delsinglesurf != surfaces.end()){
    SDL_FreeSurface(delsinglesurf->second);
    delsinglesurf++;
  }

  // Free elements of Multi-frame sprites 
  std::map<std::string, std::vector<Frame*> >::iterator delmultiframes = multiFrames.begin();
  while(delmultiframes != multiFrames.end()){
    std::vector<Frame*>::iterator delmulti = delmultiframes->second.begin();
    while(delmulti != delmultiframes->second.end()){
      delete *delmulti;
      delmulti++;
    }
    delmultiframes++;
  }

  std::map<std::string, std::vector<SDL_Surface*> >::iterator delmultisurf = multiSurfaces.begin();
  while(delmultisurf != multiSurfaces.end()){
    std::vector<SDL_Surface*>::iterator delmultis = delmultisurf->second.begin();
    while(delmultis != delmultisurf->second.end()){
      SDL_FreeSurface(*delmultis);
      delmultis++;
    }
    delmultisurf++;
  }
}

FrameFactory& FrameFactory::getInstance() {
  static FrameFactory factory;
  return factory;
}

Frame* FrameFactory::getFrame(const std::string& name) {
  std::map<std::string, Frame*>::const_iterator pos = frames.find(name); 
  // It wasn't in the map, load it.
  if ( pos == frames.end() ) {
    SDL_Surface * const surface =
      IOManager::getInstance().loadAndSet(
          gdata.getXmlStr(name+"/file"),
          gdata.getXmlBool(name+"/transparency"));
    surfaces[name] = surface;
    Frame * const frame =new Frame(name, surface);
    frames[name] = frame;
    return frame;
  }
  else {  // It was in map, just return it.
    return pos->second;
  }
}

std::vector<Frame*> FrameFactory::getFrames(const std::string& name, const double angle, const double size) {
  std::string tempname;
  std::stringstream strm;
  strm<<size;
  tempname = name+strm.str();
  // First search map to see if we've already made it:
  std::map<std::string, std::vector<Frame*> >::const_iterator 
    pos = multiFrames.find(tempname); 
  if ( pos != multiFrames.end() ) {
    return pos->second;
  }

  // It wasn't in the map, so we have to make the vector of Frames:

  SDL_Surface* surface = IOManager::
     getInstance().loadAndSet(gdata.getXmlStr(name+"/file"), gdata.getXmlBool(name+"/transparency"));
  unsigned numberOfFrames = gdata.getXmlInt(name+"/frames");
  std::vector<Frame*> frames;
  std::vector<SDL_Surface*> surfaces;
  frames.reserve(numberOfFrames);
  Uint16 srcX = gdata.getXmlInt(name+"/srcX");
  Uint16 srcY = gdata.getXmlInt(name+"/srcY");
  Uint16 width = gdata.getXmlInt(name+"/width");
  Uint16 height = gdata.getXmlInt(name+"/height");

  SDL_Surface* surf;
  SDL_Surface* surftemp;
  for (unsigned i = 0; i < numberOfFrames; ++i) {
    unsigned frameX = i * width + srcX;
    surf = ExtractSurface::getInstance().
               get(surface, width, height, frameX, srcY); 

    //SDL_Surface * rotozoomSurface (SDL_Surface *src, double angle, double zoom, int smooth);
    surftemp = rotozoomSurface(surf, angle, size, 1);

    surfaces.push_back( surftemp );
    frames.push_back( new Frame(name, surftemp, size) );
    SDL_FreeSurface(surf); 
  }
  SDL_FreeSurface(surface);   
  multiSurfaces[tempname] = surfaces;
  multiFrames[tempname] = frames;
  return frames;
}


