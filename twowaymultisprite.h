#ifndef TWOWAYMULTISPRITE__H
#define TWOWAYMULTISPRITE__H
#include <string>
#include <vector>
#include "drawable.h"
#include "vector2f.h"

class ExplodingSprite;

class TwowayMultiSprite : public Drawable {
public:
  TwowayMultiSprite(const std::string&, const std::string&, const double angle = 0, const double size = 1);
  TwowayMultiSprite(const std::string&, const std::string&, const Vector2f&, const Vector2f&, const double angle = 0, const double size = 1);
  virtual ~TwowayMultiSprite();

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    if(notreverse) return frames[currentFrame]; 
    return revframes[currentFrame]; 
  }
  void togglereverse(){ notreverse = !notreverse; }
  virtual void explode();
  bool isexploded () const {
    if(explosion) return true;
    return false;
  }
  const std::string& getName() const { return Drawable::getName(); }
  void reset();
protected:
  ExplodingSprite* explosion;
  const std::vector<Frame *> frames;
  const std::vector<Frame *> revframes;
  int worldWidth;
  int worldHeight;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int frameWidth;
  int frameHeight;
  bool notreverse;

  void advanceFrame(Uint32 ticks);
  TwowayMultiSprite(const TwowayMultiSprite&);
  TwowayMultiSprite& operator=(const TwowayMultiSprite&);
};
#endif
