#include <string>
#include "sprite.h"

class PolySprite : public Sprite {
public:
  PolySprite(const std::string&, const std::string&);

  PolySprite(const PolySprite& s);
  virtual ~PolySprite() { } 
  PolySprite& operator=(const PolySprite&);

  virtual const Frame* getFrame() const { return Sprite::getFrame(); }
  virtual void draw() const;

  virtual void update(Uint32 ticks);

private:
  int framewidth;
  int getDistance(const PolySprite*) const;
};

