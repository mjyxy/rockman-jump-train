#ifndef TWOWAYINTERMULTISPRITE__H
#define TWOWAYINTERMULTISPRITE__H
#include <string>
#include <vector>
#include "vector2f.h"
#include "twowaymultisprite.h"

class TwowayInterMultiSprite : public TwowayMultiSprite {
public:
  TwowayInterMultiSprite(const std::string&, const std::string&, const std::string&, const std::string&, const double angle = 0, const double size = 1);
  TwowayInterMultiSprite(const std::string&, const std::string&, const std::string&, const std::string&, const Vector2f&, const Vector2f&, const double angle = 0, const double size = 1);
  virtual ~TwowayInterMultiSprite() { } 

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
protected:
  const std::vector<Frame *> interframes;
  const std::vector<Frame *> revinterframes;
  int numberOfFrames;
  bool notreverse;
  int inter;
  int extend;

  void advanceFrame(Uint32 ticks);
  TwowayInterMultiSprite(const TwowayInterMultiSprite&);
  TwowayInterMultiSprite& operator=(const TwowayInterMultiSprite&);
};
#endif

