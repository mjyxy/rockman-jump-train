#include "ioManager.h"
#include "aaline.h"
#include "sprite.h"

class Health {
public:
	static Health& getInstance();  // This class is a Singleton
	void drawHealth(SDL_Surface*);
	bool empty();
	void deHealth(int);
	void reset();
	bool emptyHealth() const { return curhealth>=width; }
	void deHealth() { curhealth=width; }
private:
	Sprite box;
	int variation;
	int width;
	int height;
	int x;
	int y;
	int curhealth;
	Health( );
	Health(const Health&);
  	Health&operator=(const Health&);
};
