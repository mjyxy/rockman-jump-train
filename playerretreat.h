#include "multisprite.h"
#include "sprite.h"
#include "vector2f.h"

class Retreat : public MultiSprite { // An Retreat is interval frames;
public:
  Retreat(const std::string&, const Vector2f&); // get position
  ~Retreat();
  void draw() const ;
  virtual void update(Uint32 ticks);
  bool end(){
    return MultiSprite::isend() && sentence.isend()&&sentence2.isend() && (delay <0);
  }
private:
  Sprite ciel;
  MultiSprite sentence;
  MultiSprite sentence2;
  int delay;
  Retreat(const Retreat&); // Explicit disallow (Item 6)
  Retreat& operator=(const Retreat&); // (Item 6)
};

