#include <list>
#include <vector>
#include <SDL.h>
#include "ioManager.h"
#include "clock.h"
#include "viewport.h"
#include "hud.h"
#include "foreground.h"


class Manager {
public:
  Manager ();
  ~Manager ();
  void play();
  void switchSprite();
  void reset();
private:
  const bool env;
  const IOManager& io;
  Clock& clock;

  SDL_Surface * const screen;

  Background& background;
  Foreground& foreground;
  Story* beginstory;

  bool makeVideo;
  int frameCount;
  const std::string username;
  const std::string title;
  const int frameMax;

  void draw() const;
  void update();

  Manager(const Manager&);
  Manager& operator=(const Manager&);
  void makeFrame();
};
