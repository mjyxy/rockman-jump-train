#include "twowayintermultisprite.h"
#include "gamedata.h"
#include "frameFactory.h"

void TwowayInterMultiSprite::advanceFrame(Uint32 ticks) {
  if(inter <= 0){
    TwowayMultiSprite::update(ticks);
  }
  inter--;
  if ( X() < 0) {
    if(!notreverse){
      notreverse = true;
      inter = extend * numberOfFrames;
    }
  }
  if ( X() > worldWidth-frameWidth) {
    if(notreverse){
      notreverse = false;
      inter = extend * numberOfFrames;
    }
  }  
}

TwowayInterMultiSprite::TwowayInterMultiSprite( const std::string& name ,const std::string& name2, const std::string& name3,const std::string& name4,const double angle, const double size) :
  TwowayMultiSprite(name,name2,angle,size),
  interframes(FrameFactory::getInstance().getFrames(name3,angle,size) ),
  revinterframes(FrameFactory::getInstance().getFrames(name4,angle,size) ),
  numberOfFrames(Gamedata::getInstance().getXmlInt(name3+"/frames")),
  notreverse(true),
  inter(0),
  extend(Gamedata::getInstance().getXmlInt(name+"/extend"))
{ }

TwowayInterMultiSprite::TwowayInterMultiSprite( const std::string& name,const std::string& name2, const std::string& name3,const std::string& name4,const Vector2f& pos, const Vector2f& vel, const double angle, const double size) :

  TwowayMultiSprite(name,name2, pos, vel,angle,size),
  interframes(FrameFactory::getInstance().getFrames(name3,angle,size) ),
  revinterframes(FrameFactory::getInstance().getFrames(name4,angle,size) ),
  numberOfFrames(Gamedata::getInstance().getXmlInt(name3+"/frames")),
  notreverse(true),
  inter(0),
  extend(Gamedata::getInstance().getXmlInt(name+"/extend"))
{ }

void TwowayInterMultiSprite::draw() const { 
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  if(inter > 0){
    if( notreverse ) interframes[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else revinterframes[(inter-1)/extend]->draw(x, y);
  }else{

    TwowayMultiSprite::draw(x,y);
  }
}

void TwowayInterMultiSprite::draw(Uint32 x, Uint32 y) const { 
  if(inter > 0){
    if( notreverse ) interframes[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else revinterframes[(inter-1)/extend]->draw(x, y);
  }else{
    TwowayMultiSprite::draw(x,y);
  }
}

void TwowayInterMultiSprite::update(Uint32 ticks) { 
  advanceFrame(ticks);
}
