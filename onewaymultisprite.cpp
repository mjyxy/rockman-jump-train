#include "onewaymultisprite.h"
#include "gamedata.h"
#include "frameFactory.h"
#include "explodingSprite.h"

void OnewayMultiSprite::advanceFrame(Uint32 ticks) {
	timeSinceLastFrame += ticks;
	if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame+1) % numberOfFrames;
		timeSinceLastFrame = 0;
	}
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( X() < 0) {
    velocityX( abs( velocityX() ) );
  }
  if ( X() > worldWidth-frameWidth) {  //go back left-side
    setPosition(Vector2f(0, rand()%(worldHeight/8)));
  }  

}

OnewayMultiSprite::OnewayMultiSprite( const std::string& name , const double size) :
  Drawable(name, 
           Vector2f(0, 0), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"), 0)
           ),
  explosion(NULL),
  frames( FrameFactory::getInstance().getFrames(name, 0, size) ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval") ),
  timeSinceLastFrame( 0 ),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight())
{ 
  setPosition(Vector2f(rand()%worldWidth, rand()%(worldHeight/8)));
  setVelocity(getVelocity()*size);
  frameInterval = frameInterval/size;
}

void OnewayMultiSprite::draw() const { 
  if (explosion) {
    explosion->draw();
    return;
  }  
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  frames[currentFrame]->draw(x, y);
}

void OnewayMultiSprite::explode() { 
  if ( explosion ) return;
  Sprite sprite(getName(), getPosition(), getVelocity(), getFrame());
  explosion = new ExplodingSprite(sprite); 
}

void OnewayMultiSprite::draw(Uint32 x, Uint32 y) const { 
  if (explosion) {
    explosion->draw();
    return;
  }  
  frames[currentFrame]->draw(x, y);
}

void OnewayMultiSprite::update(Uint32 ticks) { 
  if ( explosion ) {
    explosion->update(ticks);
    if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
    }
    return;
  }  
  advanceFrame(ticks);
}
