#ifndef AROUNDMULTISPRITE__H
#define AROUNDMULTISPRITE__H
#include <string>
#include <vector>
#include "drawable.h"
#include "vector2f.h"

class ExplodingSprite;

class AroundMultiSprite : public Drawable {
public:
  AroundMultiSprite(const std::string&, const Vector2f&);
  virtual ~AroundMultiSprite() { } 

  virtual void draw() const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  void explode();
  bool isexploded () const {
    if(explosion) return true;
    return false;
  }
protected:
  ExplodingSprite* explosion;
  const std::vector<Frame *> frames;

  int worldWidth;
  int worldHeight;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int frameWidth;
  int frameHeight;
  Vector2f centre;
  float r;
  float totalang;

  void advanceFrame(Uint32 ticks);
  AroundMultiSprite(const AroundMultiSprite&);
  AroundMultiSprite& operator=(const AroundMultiSprite&);
  float getDistance(const Vector2f&) const;
};
#endif
