#include "bulletpool.h"
#include "frameFactory.h"



BulletPool& BulletPool::getInstance(){
	static BulletPool pool;
	return pool;
}

void BulletPool::update(Uint32 ticks){
	std::list<Bullet*>::iterator ptr = playerbullet.begin();
	while (ptr != playerbullet.end()) {
	    (*ptr)->update(ticks);
	    if ((*ptr)->goneTooFar()) {  // Check to see if we should free a bullet
	      	playerfreelist.push_back(*ptr);
	      	ptr = playerbullet.erase(ptr);
	    }   
	    else ++ptr;
	}
}

void BulletPool::draw(){
	std::list<Bullet*>::iterator ptr = playerbullet.begin();
	while (ptr != playerbullet.end()) {
	    (*ptr)->draw();
	    ++ptr;
	}
}

void BulletPool::playershoot(const Vector2f& pos,const Vector2f& vel, float dis){
	if(playerbullet.size() != playermaxbullet){
		std::list<Bullet*>::iterator ptr;
		if(playerfreelist.size()){
			ptr = playerfreelist.begin();
			(*ptr)->reset(pos,vel);
			playerbullet.push_back(*ptr);
			playerfreelist.erase(ptr);
		}else{
			playerbullet.push_back(new Bullet(pos, vel,
	                "playerbullet", FrameFactory::getInstance().getFrame("playerbullet"), dis));
		}
	}
	// dont need return
}

bool BulletPool::collidedWith(const Drawable* d){
	std::list<Bullet*>::iterator ptr = playerbullet.begin();
	while (ptr != playerbullet.end()) {
		if((*ptr)->collidedWith(d)){
			playerfreelist.push_back(*ptr);		// if collision, free bullet 
	      	ptr = playerbullet.erase(ptr);	
			return true;
		}
	    ++ptr;
	}
	return false;
}
