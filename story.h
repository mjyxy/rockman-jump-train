#include "sprite.h"
#include "vector2f.h"
#include "multisprite.h"

class Story { // An Retreat is interval frames;
public:
  Story(const std::string&,const std::string&,const std::string&); 
  ~Story(){}
  void draw() const ;
  void update(Uint32 ticks);
  bool end(){
    return sentence.isend() && sentence2.isend() && (delay <0);
  }
private:
  Sprite ciel;
  MultiSprite sentence;
  MultiSprite sentence2;
  MultiSprite sentence3;
  int delay;
  Story(const Story&); // Explicit disallow (Item 6)
  Story& operator=(const Story&); // (Item 6)
};

