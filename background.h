#include <string>
#include <vector>
#include "drawable.h"
#include "world.h"
#include <list>

class Background {
public:

  ~Background() { 
    std::list<Drawable*>::const_iterator ptr = far_sprites.begin();
    while ( ptr != far_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
    ptr = near_sprites.begin();
    while ( ptr != near_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
  } 
  static Background& getInstance();
  void draw() const;
  void update(Uint32 ticks);


private:
  World tree;
  std::list<Drawable*> far_sprites;
  World wall;
  std::list<Drawable*> near_sprites;

  Background();
  Background(const Background&);
  Background& operator=(const Background&);
};

