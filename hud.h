#include <iostream>
#include <string>
#include "ioManager.h"
#include "aaline.h"


class Hud {
public:
	static Hud& getInstance();  // This class is a Singleton
	void drawHUD(SDL_Surface*);
	void toggleHUD();
private:

	int timer;
	int up_bound;
	int variation;
	int width;
	int height;
	int x;
	int y;
	Hud( );
	Hud(const Hud&);
  	Hud&operator=(const Hud&);
};
