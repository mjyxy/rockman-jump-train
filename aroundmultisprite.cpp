#include "aroundmultisprite.h"
#include "gamedata.h"
#include "frameFactory.h"
#include <cmath>
#include "sprite.h"
#include "explodingSprite.h"

float const PI = 3.1415926;

void AroundMultiSprite::advanceFrame(Uint32 ticks) {
	timeSinceLastFrame += ticks;
	if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame+1) % numberOfFrames;
		timeSinceLastFrame = 0;
	}

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  float theta = (sqrt(pow(incr[0],2) + pow(incr[1],2)) / (2 * PI * r)) *360;  
  totalang+= theta;
  Vector2f delta = Vector2f( r * sin(totalang), r * cos(totalang)); 
  setPosition(centre + delta);

}

AroundMultiSprite::AroundMultiSprite( const std::string& name , const Vector2f& pos) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  explosion(NULL),
  frames( FrameFactory::getInstance().getFrames(name) ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval") ),
  timeSinceLastFrame( 0 ),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  centre(pos),
  r(getDistance(centre)),
  totalang(0)
{ 

  // check position < 0 
  if(X() < 0) X(0);

}

void AroundMultiSprite::explode() { 
  if ( explosion ) return;
  Sprite sprite(getName(), getPosition(), getVelocity(), getFrame());
  explosion = new ExplodingSprite(sprite); 
}


void AroundMultiSprite::draw() const { 
  if (explosion) {
    explosion->draw();
    return;
  } 
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  frames[currentFrame]->draw(x, y);
  // If run next command, there will be problem!
  // frames[currentFrame]->draw(x, y, 45);
}

void AroundMultiSprite::update(Uint32 ticks) { 
  if ( explosion ) {
    explosion->update(ticks);
    if ( explosion->chunkCount() == 0 ) {
      delete explosion;
      explosion = NULL;
    }
    return;
  } 
   
  advanceFrame(ticks);
}


float AroundMultiSprite::getDistance(const Vector2f& pos) const { 
  return hypot(X()-pos[0], Y()-pos[1]);
}
