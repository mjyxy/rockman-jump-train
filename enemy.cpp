#include "enemy.h"
#include "gamedata.h"
#include "frameFactory.h"

void Enemy::advanceFrame(Uint32 ticks) {
  if(inter <= 0){
    TwowayMultiSprite::update(ticks);
  }
  inter--;
  if ( X() < centra - radius) {
    if(!notreverse){
      notreverse = true;
      togglereverse();
      inter = extend * numberOfFrames;
    }
    X(centra - radius);
    velocityX( abs( velocityX() ) );
  }
  if ( X() > centra + radius - frameWidth) {
    if(notreverse){
      notreverse = false;
      togglereverse();
      inter = extend * numberOfFrames;
    }
    X(centra + radius - frameWidth);
    velocityX( -abs( velocityX() ) );
  }  
}

void Enemy::reset(){
  setPosition(Vector2f(centra-radius, Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/Locy")));
  setVelocity(Vector2f(Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"),
                    Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedY")));

  notreverse = true;
  inter = 0;

  TwowayMultiSprite::reset();
}

Enemy::Enemy( const std::string& name ,const std::string& name2, const std::string& name3,const std::string& name4, int c, int r) :
  TwowayMultiSprite(name,name2,Vector2f(c-r, Gamedata::getInstance().getXmlInt(name+"/Locy")),
    Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
  ),
  interframes(FrameFactory::getInstance().getFrames(name3) ),
  revinterframes(FrameFactory::getInstance().getFrames(name4) ),
  numberOfFrames(Gamedata::getInstance().getXmlInt(name3+"/frames")),
  notreverse(true),
  inter(0),
  extend(Gamedata::getInstance().getXmlInt(name+"/extend")),
  centra(c),
  radius(r)
{ }

void Enemy::draw() const { 
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  if(inter > 0){
    if( notreverse ) interframes[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else revinterframes[(inter-1)/extend]->draw(x, y);
  }else{
    TwowayMultiSprite::draw(x,y);
  }
}

void Enemy::draw(Uint32 x, Uint32 y) const { 
  if(inter > 0){
    if( notreverse ) interframes[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else revinterframes[(inter-1)/extend]->draw(x, y);
  }else{
    TwowayMultiSprite::draw(x,y);
  }
}

void Enemy::update(Uint32 ticks) { 
  advanceFrame(ticks);
}
