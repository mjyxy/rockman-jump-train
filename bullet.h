#ifndef BULLET__H
#define BULLET__H
#include "sprite.h"
#include "gamedata.h"

class CollisionStrategy;
class Bullet : public Sprite {
public:
  explicit Bullet(
    const Vector2f& pos, const Vector2f vel, 
    const std::string& name, const Frame* fm, float dis=-1 );
  ~Bullet();
  virtual void update(Uint32 ticks);
  bool goneTooFar() const { return tooFar; }
  void reset(const Vector2f& pos, const Vector2f vel) {
    setPosition(pos);
    setVelocity(vel);
    tooFar = false;
    distance = 0;
  }
  bool collidedWith(const Drawable*) const;
private:
  CollisionStrategy * strategy;
  float distance;
  float maxDistance;
  bool tooFar;
  Bullet(const Bullet&);
  Bullet& operator=(const Bullet&);
};
#endif
