#include "foreground.h"
#include "onewaymultisprite.h"
#include "sprite.h"
#include "polysprite.h"
#include "polymultisprite.h"
#include "bulletpool.h"
#include "enemy.h"
#include "smartenemy.h"
#include "health.h"

Foreground& Foreground::getInstance() {
  	static Foreground instance; 
  	return instance;
}

void Foreground::reset() {
  // reset play, health, enemylist and bulltpool
  player.reset();
  std::list<Drawable*>::const_iterator ptr = enemy_sprites.begin();
  while ( ptr != enemy_sprites.end() ) {
    if(dynamic_cast<Enemy* >(*ptr)) dynamic_cast<Enemy* >(*ptr)->reset();
    else dynamic_cast<SmartEnemy* >(*ptr)->reset();
    ptr++;
  }
  Health::getInstance().reset();
  BulletPool::getInstance().reset();
  if(endstory) delete endstory;
  endstory = NULL;
}


Foreground::Foreground() :  viewport( Viewport::getInstance() ),
  endstory(NULL),
  behind_sprites(),
  player("rockman"),
  palyershoot(false),
  enemyexplodenum(0),
  enemy_sprites(),
  trap_sprites(),
  front_sprites()

{

  viewport.setObjectToTrack( &player);
  behind_sprites.push_back( new PolySprite("ele1","trainbody") );

  behind_sprites.push_back( new PolySprite("trainbody","trainbody") );
  behind_sprites.push_back( new PolyMultiSprite("ele_effect","trainbody") );
  behind_sprites.push_back( new PolyMultiSprite("ele_effect2","trainbody") );
  behind_sprites.push_back( new Sprite("trainhead") );
  behind_sprites.push_back( new Sprite("traintail") );
  for(int i=0;i<Gamedata::getInstance().getXmlInt("enemy/num");i++){ 
    enemy_sprites.push_back( new Enemy("enemy","enemy2","inter1","inter2", Gamedata::getInstance().getXmlInt("enemy/init")+Gamedata::getInstance().getXmlInt("enemy/factor")*i, Gamedata::getInstance().getXmlInt("enemy/radius")) );
  }

  for(int i=0;i<Gamedata::getInstance().getXmlInt("smartenemy/num");i++){ 
    enemy_sprites.push_back( new SmartEnemy("smartenemy","smartenemy2","smartenemyimpact",
      "smartenemyimpact2", Gamedata::getInstance().getXmlInt("smartenemy/init")+Gamedata::getInstance().getXmlInt("smartenemy/factor")*i, 
      Gamedata::getInstance().getXmlInt("smartenemy/radius"), player) );
  }
  trap_sprites.push_back( new PolySprite("ele_trap","trainbody") );
  trap_sprites.push_back( new PolySprite("ele_trap2","trainbody") );
  front_sprites.push_back( new Sprite("ladder") ); 
  front_sprites.push_back( new PolySprite("ele2","trainbody") );
  for(int i=0;i<Gamedata::getInstance().getXmlInt("bird4/num");i++){
    front_sprites.push_back( new OnewayMultiSprite("bird4",Gamedata::getInstance().getXmlFloat("bird4/factor")*i+Gamedata::getInstance().getXmlFloat("bird4/index")));
  }
}


void Foreground::draw() const {
  std::list<Drawable*>::const_iterator ptr = trap_sprites.begin();
  
  while ( ptr != trap_sprites.end() ) {
    (*ptr)->draw();
    ++ptr;
  }
	ptr = behind_sprites.begin();
	while ( ptr != behind_sprites.end() ) {
  	(*ptr)->draw();
  	++ptr;
	}

  player.draw();
  BulletPool::getInstance().draw();
  ptr = enemy_sprites.begin();
  while ( ptr != enemy_sprites.end() ) {
    (*ptr)->draw();
    ++ptr;
  }  

	ptr = front_sprites.begin();
	while ( ptr != front_sprites.end() ) {
  	(*ptr)->draw();
  	++ptr;
	}
  if(endstory) endstory->draw();
  Health::getInstance().drawHealth(IOManager::getInstance().getScreen() );
}
void Foreground::update(Uint32 ticks){

	std::list<Drawable*>::const_iterator ptr = behind_sprites.begin();
	while ( ptr != behind_sprites.end() ) {
  	(*ptr)->update(ticks);
  	++ptr;
	}
  std::list<Drawable*>::iterator ptr2 = behind_sprites.begin();
  ptr2++;
  player.update(ticks,*ptr2);

  BulletPool::getInstance().update(ticks);
  ptr = enemy_sprites.begin();
  while ( ptr != enemy_sprites.end() ) {
    if(!(*ptr)->isexploded()){
      if(BulletPool::getInstance().collidedWith(*ptr)){
        explodesound();
        (*ptr)->explode();
      }
    }
    (*ptr)->update(ticks);
    if(!(*ptr)->isexploded()){
      if(player.collidedWithEnemy(*ptr)){
        if(dynamic_cast<Enemy* >(*ptr)) Health::getInstance().deHealth(6);
        else Health::getInstance().deHealth(8);
        
        if(Health::getInstance().emptyHealth()) player.explode();
        else player.fullgodtimer();
      }
    }
    ++ptr;
  } 
  ptr = trap_sprites.begin();
  while ( ptr != trap_sprites.end() ) {
    (*ptr)->update(ticks);
    if(player.collidedWithTrap(*ptr)){  // ele explode
      Health::getInstance().deHealth();
      player.eleexplode();
      player.reposition(); // set restart position
    }
    ++ptr;
  }
  ptr = front_sprites.begin();
  if(player.collidedWithLadder(*ptr)){
    // win
    if(!endstory) endstory = new Story("end1","end2","end3");
    if(!player.isgod()) player.god();
  }
  if(endstory) endstory->update(ticks);
	while ( ptr != front_sprites.end() ) {
  	(*ptr)->update(ticks);
  	++ptr;
	}
  viewport.update(); // always update viewport last

}

