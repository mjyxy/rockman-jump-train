#ifndef MULTISPRITE__H
#define MULTISPRITE__H
#include <string>
#include <vector>
#include "drawable.h"

class ExplodingSprite;

class MultiSprite : public Drawable {
public:
  MultiSprite(const std::string&, const double angle = 0, const double size = 1);
  MultiSprite(const std::string&, const Vector2f&, const Vector2f&, const double angle = 0, const double size = 1);
  virtual ~MultiSprite() { } 

  virtual void draw() const;
  void draw(const Uint32, const Uint32) const;
  void drawsentence() const;
  void drawsentenceinview() const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  bool isend() const { if(currentFrame == (numberOfFrames-1)) return true; return false; }
  void setcurrentFrame(unsigned n) { currentFrame=n; }
  void explode();
  bool isexploded () const {
    if(explosion) return true;
    return false;
  }
protected:
  ExplodingSprite* explosion;
  const std::vector<Frame *> frames;
  int worldWidth;
  int worldHeight;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int frameWidth;
  int frameHeight;

  void advanceFrame(Uint32 ticks);
  MultiSprite(const MultiSprite&);
  MultiSprite& operator=(const MultiSprite&);
};
#endif
