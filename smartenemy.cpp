#include "smartenemy.h"
#include "gamedata.h"
#include "frameFactory.h"

void SmartEnemy::advanceFrame(Uint32 ticks) {
  if(currentMode == NORMAL){
    if(findplayer()>0){
      currentMode = IMPACT;
      inter = extend * numberOfFrames;
      impactright();
    }else if(findplayer()<0){
      currentMode = IMPACT;
      inter = extend * numberOfFrames;
      impactleft();
    }else{
      TwowayMultiSprite::update(ticks);
      if ( X() < centra - radius) {
        if(!notreverse){
          notreverse = true;
          togglereverse();
        }
        X(centra - radius);
        velocityX( abs( velocityX() ) );
      }
      if ( X() > centra + radius - frameWidth) {
        if(notreverse){
          notreverse = false;
          togglereverse();
        }
        X(centra + radius - frameWidth);
        velocityX( -abs( velocityX() ) );
      } 
    }
  }
  if(currentMode == IMPACT){
    if(!isexploded ()) inter--;
    else{
      inter = -1; // finish impact
    }
    TwowayMultiSprite::update(ticks);
    if(inter<0){  // go back normal after go back to seek range
      if ( X() < centra - radius) {
        if(!notreverse){
          notreverse = true;
          togglereverse();
        }
        velocityX( Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"));
      }else if ( X() > centra + radius - frameWidth) {
        if(notreverse){
          notreverse = false;
          togglereverse();
        }
        else velocityX( - Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"));
      }else{
        currentMode = NORMAL;
        if(notreverse) velocityX( Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"));
        else velocityX( - Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"));
      }
    }
  } 
}
void SmartEnemy::impactright(){//move right fast
  setVelocity(Vector2f(impactSpeed,0));
}

void SmartEnemy::impactleft(){//move left fast
  setVelocity(Vector2f(-impactSpeed,0));
}

void SmartEnemy::reset(){
  setPosition(Vector2f(centra-radius, Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/Locy")));
  setVelocity(Vector2f(Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedX"),
                    Gamedata::getInstance().getXmlInt(TwowayMultiSprite::getName()+"/speedY")));

  notreverse = true;
  inter = 0;
  currentMode = NORMAL;
  TwowayMultiSprite::reset();
}

SmartEnemy::SmartEnemy( const std::string& name ,const std::string& name2, const std::string& name3,const std::string& name4, int c, int r, const Player& p) :
  TwowayMultiSprite(name,name2,Vector2f(c-r, Gamedata::getInstance().getXmlInt(name+"/Locy")),
    Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
  ),
  player(p),
  seekDistance(Gamedata::getInstance().getXmlFloat(name3+"/seekdistance")),
  impactSpeed(Gamedata::getInstance().getXmlInt(name3+"/impactspeed")),
  currentMode(NORMAL),
  left_impact_frames(FrameFactory::getInstance().getFrames(name3) ),
  right_impact_frames(FrameFactory::getInstance().getFrames(name4) ),
  numberOfFrames(Gamedata::getInstance().getXmlInt(name3+"/frames")),
  notreverse(true),
  inter(0),
  extend(Gamedata::getInstance().getXmlInt(name+"/extend")),
  centra(c),
  radius(r)
{ }

void SmartEnemy::draw() const { 
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());
  if(TwowayMultiSprite::isexploded() || inter < 1){
    TwowayMultiSprite::draw(x,y); 
  }else{
    if( notreverse ) left_impact_frames[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else right_impact_frames[(inter-1)/extend]->draw(x, y);
  }
}

void SmartEnemy::draw(Uint32 x, Uint32 y) const { 
  if(TwowayMultiSprite::isexploded() || inter < 1){
    TwowayMultiSprite::draw(x,y); 
  }else{
    if( notreverse ) left_impact_frames[(extend * numberOfFrames-inter)/extend]->draw(x, y);
    else right_impact_frames[(inter-1)/extend]->draw(x, y);
  }
}

void SmartEnemy::update(Uint32 ticks) { 
  advanceFrame(ticks);
}
