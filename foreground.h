#include "background.h"
#include "player.h"
#include "story.h"

class Foreground {
public:

  ~Foreground() { 
    std::list<Drawable*>::const_iterator ptr = behind_sprites.begin();
    while ( ptr != behind_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
    ptr = enemy_sprites.begin();
    while ( ptr != enemy_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
    ptr = trap_sprites.begin();
    while ( ptr != trap_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
    ptr = front_sprites.begin();
    while ( ptr != front_sprites.end() ) {
      delete (*ptr);
      ++ptr;
    }
    if(endstory) delete endstory;
  } 
  static Foreground& getInstance();
  void draw() const;
  void update(Uint32 ticks);
  void playerjump() { player.jump(); }
  void playermoveright() { player.moveright(); }
  void playermoveleft() { player.moveleft(); }
  void playernotmove() { player.notmove(); }
  void playershoot() { player.shoot(); }
  void playerstopshoot() { player.stopshoot(); }
  void playergod() { player.god(); }

  void shootsound() { palyershoot=true; }
  bool isshoot() {  return palyershoot; }
  void falseshoot() { palyershoot=false; }

  void explodesound() { enemyexplodenum++; }
  int enemyexplode() { return enemyexplodenum; }
  void explodedone() { enemyexplodenum--; }

  void reset();
private:
  Viewport& viewport;
  Story* endstory;
  std::list<Drawable*> behind_sprites;
  Player player;
  bool palyershoot;
  int enemyexplodenum;
  std::list<Drawable*> enemy_sprites;
  std::list<Drawable*> trap_sprites;
  std::list<Drawable*> front_sprites;


  Foreground();
  Foreground(const Foreground&);
  Foreground& operator=(const Foreground&);

};

