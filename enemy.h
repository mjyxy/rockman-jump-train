#include <string>
#include <vector>
#include "vector2f.h"
#include "twowaymultisprite.h"

class Enemy : public TwowayMultiSprite {
public:
  Enemy(const std::string&, const std::string&, const std::string&, const std::string&,  int, int );
  virtual ~Enemy() { } 

  virtual void draw() const;
  virtual void draw(const Uint32, const Uint32) const;
  virtual void update(Uint32 ticks);
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  void reset();
protected:

  const std::vector<Frame *> interframes;
  const std::vector<Frame *> revinterframes;
  int numberOfFrames;
  bool notreverse;
  int inter;
  int extend;
  
  int centra;
  int radius;
  
  void advanceFrame(Uint32 ticks);
  Enemy(const Enemy&);
  Enemy& operator=(const Enemy&);
};
