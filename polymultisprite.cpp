#include <cmath>
#include "polymultisprite.h"
#include "gamedata.h"
#include "frameFactory.h"
#include "viewport.h"

PolyMultiSprite::PolyMultiSprite(const std::string& name, const std::string& name2):
  MultiSprite(name),
  framewidth(Gamedata::getInstance().getXmlInt(name2+"/width"))
{ }

void PolyMultiSprite::draw() const { 
  MultiSprite::draw(); 
  MultiSprite::draw(X()+framewidth,Y());
}

int PolyMultiSprite::getDistance(const PolyMultiSprite *obj) const { 
  return hypot(X()-obj->X(), Y()-obj->Y());
}

void PolyMultiSprite::update(Uint32 ticks) { 

  float tempX = Viewport::getInstance().X();

  while(X()<((int)tempX) % framewidth){
    X(X()+framewidth);
  }
  X(((int)X()%framewidth)-((int)tempX) % framewidth+tempX);
  MultiSprite::update(ticks);

}
