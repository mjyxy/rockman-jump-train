#include "gamedata.h"
#include "bulletpool.h"
#include "playerretreat.h"
#include "foreground.h"
#include "health.h"

Player::~Player(){  
  if(explosion) delete explosion;
  delete strategy1; 
  delete strategy2; 
} 


void Player::standadvanceFrame(Uint32 ticks) {  //stand don't change x position
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > standframeInterval) {
    currentFrame = (currentFrame+1) % numberOfstandFrames;
    timeSinceLastFrame = 0;
  }
  if(isshoot){
    bullettime += ticks;
    if (bullettime > bulletInterval) {
      Vector2f pos = getPosition();
      Vector2f vel = Vector2f(movespeed* 1.5,0);
      if(preright){
        pos = pos + Vector2f(60,50);
        vel = vel *(-1);
      }else{
        pos = pos + Vector2f(0,50);
        vel = vel *(1);
      }
      BulletPool::getInstance().playershoot(pos,vel);
      Foreground::getInstance().shootsound();
      bullettime = 0;
    }

  }
}

void Player::walkadvanceFrame(Uint32 ticks, const Drawable* d) {
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > walkframeInterval) {
    currentFrame = (currentFrame+1) % numberOfwalkFrames;
    timeSinceLastFrame = 0;
  }

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }

  int temp = collidedWithSide(d);
  if(temp != 0){
    X(X()+temp);
  } 

  if(isshoot){
    bullettime += ticks;
    if (bullettime > bulletInterval) {
      Vector2f pos = getPosition();
      Vector2f vel = Vector2f(movespeed * 1.5, 0);
      if(preright){
        pos = pos + Vector2f(69,50);
        vel = vel *(-1);
      }else{
        pos = pos + Vector2f(0,50);
        vel = vel *(1);
      }
      BulletPool::getInstance().playershoot(pos,vel);
      Foreground::getInstance().shootsound();
      bullettime = 0;
    }
  }
}

void Player::jumpadvanceFrame(Uint32 ticks, const Drawable* d) {  //stand don't change x position
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > jumpframeInterval) {
    currentFrame = (currentFrame+1) % numberOfjumpFrames;
    if(currentFrame == 0) currentFrame = numberOfjumpFrames - 1;
    timeSinceLastFrame = 0;
  }

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( Y() < 0) {
    Y(0);
    velocityY( 0 );
  }

  int temp2 = collidedWithSide(d);
  if(temp2 != 0){
    X(X()+temp2);
  } 

  int temp = collidedWith2(d);
  if(temp && velocityY() > 0){  
    Y(Y()-(90-temp));
    isjump = false;
    velocityY( 0 );
  }

  if( Y() > worldHeight-jumpframeHeight){
    Y(worldHeight-jumpframeHeight);
    isjump = false;
    velocityY( 0 );
  }

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }
     
  int index = 0;
  switch(currentFrame) {
    case 0: index = 42; break;
    case 1: index = 32; break;
    case 2: index = 28; break;
    case 3: index = 32; break;
    case 4: index = 30; break;
    case 5: index = 38; break;
    default : index = 30; break;
  }
  if(isshoot){
    bullettime += ticks;
    if (bullettime > bulletInterval) {
      Vector2f pos = getPosition();
      Vector2f vel = Vector2f(movespeed * 1.5, 0);
      if(preright){
        pos = pos + Vector2f(63,index);
        vel = vel *(-1);
      }else{
        pos = pos + Vector2f(0,index);
        vel = vel *(1);
      }
      BulletPool::getInstance().playershoot(pos,vel);
      Foreground::getInstance().shootsound();
      bullettime = 0;
    }
  }
}

void Player::resetflag(){
  currentFrame = 0;
  timeSinceLastFrame = 0;
  bullettime = 0;
  imurfather = false;
  needtoreposition = false;
  preright = true;  // face to right
  isshoot = false;
  isjump = 0;   // used to save jump state
  prestate = 0;   // used to save prestate, 0 stand, 1 walk, 2 jump. For pause
  ispause = false;    // pause flag
  isfall = false;
  godtimer = 0;
  setVelocity(Vector2f(0,0));
}

void Player::reset() { 

  if ( explosion ){
    delete explosion;
    explosion = NULL;
  }
  setPosition(Vector2f(Gamedata::getInstance().getXmlInt(getName()+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(getName()+"/startLoc/y")));
  resetflag();
}

void Player::explode() { 
  if ( explosion ) return;
  if(preright) explosion = new Retreat("retreat",getPosition()); 
  else explosion = new Retreat("revretreat",getPosition()); 
}

void Player::eleexplode() { 
  if ( explosion ) return;
  if(preright) explosion = new Retreat("eleretreat",getPosition()); 
  else explosion = new Retreat("reeleretreat",getPosition()); 
}

void Player::falladvanceFrame(Uint32 ticks, const Drawable* d) {  //stand don't change x position

  velocityY( -jumpspeed );
  currentFrame = 3;

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }   

  int temp2 = collidedWithSide(d);
  if(temp2 != 0){
    X(X()+temp2);
  }

  int temp = collidedWith2(d);
  if(temp){
    Y(Y()-(90-temp));
    velocityY( 0 );
    isfall = false;
  }
  if( Y() > worldHeight-jumpframeHeight){
    Y(worldHeight-jumpframeHeight);
    isfall = false;
    velocityY( 0 );
  }

  if(isshoot){
    bullettime += ticks;
    if (bullettime > bulletInterval) {
      Vector2f pos = getPosition();
      Vector2f vel = Vector2f(movespeed * 1.5, 0);
      if(preright){
        pos = pos + Vector2f(63,28);
        vel = vel *(-1);
      }else{
        pos = pos + Vector2f(0,28);
        vel = vel *(1);
      }
      BulletPool::getInstance().playershoot(pos,vel);
      Foreground::getInstance().shootsound();
      bullettime = 0;
    }
  }

}

void Player::draw() const { 
  if (explosion) {
    explosion->draw();
    return;
  }
  Uint32 x = static_cast<Uint32>(X());
  Uint32 y = static_cast<Uint32>(Y());

  if(!ispause){
    if(!(godtimer%4)){
      if(isjump){ //jump
        if(isshoot) preright? right_jump_shoot_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_shoot_frames[currentFrame % numberOfjumpFrames]->draw(x, y); 
        else preright? right_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y); 
      }else{ // stand or walk
        if(isfall){
          if(isshoot) preright? right_jump_shoot_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_shoot_frames[currentFrame % numberOfjumpFrames]->draw(x, y); 
          else preright? right_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y); 
        }else{
          if(!velocityX()){//stand
            if(isshoot) preright? stand_shoot_frames[0]->draw(x,y) : stand_shoot_frames[1]->draw(x,y); 
            else preright? right_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y) : left_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y);
          }else if( velocityX() > 0){ // walk right
            if(isshoot) right_walk_shoot_frames[currentFrame % numberOfwalkFrames]->draw(x, y); 
            else right_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y); 
          }else{  // walk left
            if(isshoot) left_walk_shoot_frames[currentFrame % numberOfwalkFrames]->draw(x, y); 
            else left_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y) ;
          }
        }
      }
    }
  }else{

    switch (prestate){
      case 0: // stand
        preright? right_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y) : left_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y);
        break;
      case 1:
        preright? right_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y) : left_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y);
        break;
      case 2:
        preright? right_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y);
        break;
      default: break;
    }    
  }
}


void Player::update(Uint32 ticks, const Drawable* d) { 
  if ( explosion ) {
    explosion->update(ticks);
    if ( explosion->end() ) {
      delete explosion;
      Health::getInstance().reset();
      if(needtoreposition){
        resetflag();
        X((X()/642)*642+130); Y(245);
      }
      explosion = NULL;
      godtimer = 120;
    }
    return;
  }

  if(ticks){
    ispause = false;
    if(godtimer) godtimer--;
    if(isjump){
      if(isjump == 2){  // initial
        currentFrame = 0;
        timeSinceLastFrame = 0;
        isjump = 1;
        velocityY( jumpspeed );
      }   
      prestate = 2;     // 2 for jump
      velocityY( velocityY() + jumpacc);
      jumpadvanceFrame(ticks,d);
      if(velocityX()>0) preright = true;
      if(velocityX()<0) preright = false; 

    }else{
      if(!collidedWith2(d) && Y()+jumpframeHeight+5 < worldHeight){
        isfall = true;
      }

      if(isfall){
        prestate = 3; //fall or maybe unness
        if(velocityX()>0){
          preright = true;
        }
        if(velocityX()<0){
          preright = false;
        }
        falladvanceFrame(ticks,d);
      }else{      
        if(!velocityX()){ //stand
          if(prestate == 1){
            currentFrame = 0;
          }
          prestate = 0;    // 0 for stand
          standadvanceFrame(ticks);
        }else{
          if(prestate == 0){
            currentFrame = 0;
          }
          prestate = 1; // 1 for walk
          if(velocityX()>0){
            preright = true;
            walkadvanceFrame(ticks,d);

          }else if(velocityX()<0){    // left
            preright = false;
            walkadvanceFrame(ticks,d);
          }
        }
      }
    }


  }else{
    ispause = true;
  }
}



Player::Player( const std::string& name , const double angle, const double size) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  explosion(NULL),
  right_stand_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightstand"),angle,size) ),
  right_walk_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightwalk"),angle,size) ),
  right_jump_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightjump"),angle,size) ),

  left_stand_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftstand"),angle,size) ),
  left_walk_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftwalk"),angle,size) ),
  left_jump_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftjump"),angle,size) ),

   
  stand_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/stand_shoot"),angle,size) ),
  right_walk_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightwalk_shoot"),angle,size) ),
  left_walk_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftwalk_shoot"),angle,size) ), 
  right_jump_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightjump_shoot"),angle,size) ),
  left_jump_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftjump_shoot"),angle,size) ), 

  standcollisionframe( FrameFactory::getInstance().getFrame(Gamedata::getInstance().getXmlStr(name+"/standcollisionframe")) ),
  jumpcollisionframe( FrameFactory::getInstance().getFrame(Gamedata::getInstance().getXmlStr(name+"/jumpcollisionframe")) ),

  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfstandFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightstand")+"/frames") ),
  numberOfwalkFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightwalk")+"/frames") ),
  numberOfjumpFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightjump")+"/frames") ),

  standframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightstand")+"/frameInterval") ),
  walkframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightwalk")+"/frameInterval") ),
  jumpframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightjump")+"/frameInterval") ),
  bulletInterval( Gamedata::getInstance().getXmlInt(name+"/bulletInterval") ),

  timeSinceLastFrame( 0 ),
  bullettime( 0 ),
  walkframeWidth(right_walk_frames[0]->getWidth()),
  jumpframeHeight(right_jump_frames[0]->getHeight()),
  preright(true),
  isshoot(false),
  imurfather(false),
  needtoreposition(false),
  isjump(0),

  prestate(0),
  ispause(false),
  movespeed(Gamedata::getInstance().getXmlInt(name+"/movespeed")),
  jumpspeed(Gamedata::getInstance().getXmlInt(name+"/jumpspeed")),
  jumpacc(Gamedata::getInstance().getXmlInt(name+"/jumpacc")),
  godtimer(0),
  isfall(false),
  strategy1( new PerPixelCollisionStrategy ),
  strategy2( new MyCollisionStrategy )
{ }

Player::Player( const std::string& name, const Vector2f& pos, const Vector2f& vel, const double angle, const double size) :
  Drawable(name, pos, vel),
  explosion(NULL),
  right_stand_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightstand"),angle,size) ),
  right_walk_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightwalk"),angle,size) ),
  right_jump_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightjump"),angle,size) ),

  left_stand_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftstand"),angle,size) ),
  left_walk_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftwalk"),angle,size) ),
  left_jump_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftjump"),angle,size) ),

  stand_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/stand_shoot"),angle,size) ),
  right_walk_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightwalk_shoot"),angle,size) ),
  left_walk_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftwalk_shoot"),angle,size) ), 
  right_jump_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/rightjump_shoot"),angle,size) ),
  left_jump_shoot_frames( FrameFactory::getInstance().getFrames(Gamedata::getInstance().getXmlStr(name+"/leftjump_shoot"),angle,size) ),   

  standcollisionframe( FrameFactory::getInstance().getFrame(Gamedata::getInstance().getXmlStr(name+"/standcollisionframe")) ),
  jumpcollisionframe( FrameFactory::getInstance().getFrame(Gamedata::getInstance().getXmlStr(name+"/jumpcollisionframe")) ), 


  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),

  currentFrame(0),
  numberOfstandFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightstand")+"/frames") ),
  numberOfwalkFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightwalk")+"/frames") ),
  numberOfjumpFrames( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightjump")+"/frames") ),

  standframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightstand")+"/frameInterval") ),
  walkframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightwalk")+"/frameInterval") ),
  jumpframeInterval( Gamedata::getInstance().getXmlInt(Gamedata::getInstance().getXmlStr(name+"/rightjump")+"/frameInterval") ),
  bulletInterval( Gamedata::getInstance().getXmlInt(name+"/bulletInterval") ),

  timeSinceLastFrame( 0 ),
  bullettime( 0 ),
  walkframeWidth(right_walk_frames[0]->getWidth()),
  jumpframeHeight(right_jump_frames[0]->getHeight()),
  preright(true),
  isshoot(false),
  imurfather(false),
  needtoreposition(false),
  isjump(0),

  prestate(0),
  ispause(false),
  movespeed(Gamedata::getInstance().getXmlInt(name+"/movespeed")),
  jumpspeed(Gamedata::getInstance().getXmlInt(name+"/jumpspeed")),
  jumpacc(Gamedata::getInstance().getXmlInt(name+"/jumpacc")),
  godtimer(0),
  isfall(false),
  strategy1( new PerPixelCollisionStrategy ),
  strategy2( new MyCollisionStrategy )
{ }

void Player::update(Uint32 ticks) {   // useless
  if(ticks){
    ispause = false;
    if(isjump){
      if(isjump == 2){  // initial
        currentFrame = 0;
        timeSinceLastFrame = 0;
        isjump = 1;
        velocityY( jumpspeed );
      }   
      prestate = 2;     // 2 for jump
      velocityY( velocityY() + jumpacc);
      jumpadvanceFrame(ticks);
      if(velocityX()>0) preright = true;
      if(velocityX()<0) preright = false; 
    }else{
      if(X()>642 && X() <772 && Y()<367){
        isfall =true;
      }
      if(isfall){
        falladvanceFrame(ticks);
        if(Y()>367) isfall=false;
        velocityY(0);
      }else{
        if(!velocityX()){ //stand
          if(prestate == 1){
            currentFrame = 0;
          }
          prestate = 0;    // 0 for stand
          standadvanceFrame(ticks);
        }else{
          if(prestate == 0){
            currentFrame = 0;
          }
          prestate = 1; // 1 for walk
          if(velocityX()>0){
            preright = true;
            walkadvanceFrame(ticks);

          }else if(velocityX()<0){    // left
            preright = false;
            walkadvanceFrame(ticks);
          }
        }
      }
    }

    if(Y() >245){
      if(X()<642){
        X(643);
      }
      if(X()>712){
        X(712);
      }
    }

  }else{
    ispause = true;
  }
}

void Player::moveright(){
  velocityX( movespeed );
}

void Player::moveleft(){
  velocityX( -movespeed );
}
void Player::notmove(){
  velocityX( 0 );
}

void Player::jump(){  // give player a jump move (from -y to y)  move  to update to care pause
  if(!isjump && !isfall){
    isjump = 2;
  }
}

void Player::shoot(){  
  isshoot = true;
}
void Player::stopshoot(){ 
  isshoot = false;
  if(bullettime > 32){     // maybe set as half interval
    Vector2f pos = getPosition();
    Vector2f vel = Vector2f(movespeed * 1.5, 0);
    if(preright){
      pos = pos + Vector2f(69,50);
      vel = vel *(-1);
    }else{
      pos = pos + Vector2f(0,50);
      vel = vel *(1);
    }
    BulletPool::getInstance().playershoot(pos,vel);
    Foreground::getInstance().shootsound();
  }
  bullettime = 0;
}

void Player::walkadvanceFrame(Uint32 ticks) {
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > walkframeInterval) {
    currentFrame = (currentFrame+1) % numberOfwalkFrames;
    timeSinceLastFrame = 0;
  }

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }  
}

void Player::jumpadvanceFrame(Uint32 ticks) {  //stand don't change x position
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > jumpframeInterval) {
    currentFrame = (currentFrame+1) % numberOfjumpFrames;
    if(currentFrame == 0) currentFrame = numberOfjumpFrames - 1;
    timeSinceLastFrame = 0;
  }

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( Y() < 0) {
    Y(0);
    velocityY( 0 );
  }

  if ( X()<641 && Y() > 244 && velocityY() > 0) {        // problem right now equal to starloc 
    isjump = false;
    velocityY( 0 );
  }   
  if(X()>642 && X() <772 && Y()>367){
    isjump = false;
    Y(367);
  }

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }   
}

void Player::falladvanceFrame(Uint32 ticks) {  //stand don't change x position
  velocityY( -jumpspeed );
  currentFrame = 3;
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr); 

  if ( X() < 0) {
    X(0);
  }
  if ( X() > worldWidth-walkframeWidth) {
    X(worldWidth-walkframeWidth);
  }   
}



void Player::draw(Uint32 x, Uint32 y) const { 

  if(!ispause){
    if(isjump){ //jump
      preright? right_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y); 
    }else{  // stand or walk
      if(!velocityX()){//stand
        preright? right_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y) : left_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y);
      }else if( velocityX() > 0){ // walk right
        right_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y); 
      }else{  // walk left
        left_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y) ;
      }
    }
  }else{
    switch (prestate){
      case 0: // stand
        preright? right_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y) : left_stand_frames[currentFrame % numberOfstandFrames]->draw(x, y);
        break;
      case 1:
        preright? right_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y) : left_walk_frames[currentFrame % numberOfwalkFrames]->draw(x, y);
        break;
      case 2:
        preright? right_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y) : left_jump_frames[currentFrame % numberOfjumpFrames]->draw(x, y);
        break;
      default: break;
    }    
  }
}
